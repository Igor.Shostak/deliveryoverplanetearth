package com.ishostak.dope.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "manager")
public class Manager implements Serializable {

    @Id
    @Column(name = "id_manager")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idManager;

    @NotNull
    boolean isManager;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @MapsId
    @JoinColumn(name = "id_user", insertable = false, updatable = false)
    private User user;

    public boolean isManager() {
        return isManager;
    }

    public void setManager(boolean manager) {
        isManager = manager;
    }

    public com.ishostak.dope.models.User getUser() {
        return user;
    }

    public void setUser(com.ishostak.dope.models.User user) {
        user = user;
    }
}
