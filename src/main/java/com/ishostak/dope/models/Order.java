package com.ishostak.dope.models;

import com.ishostak.dope.Util.OrderState;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "orders")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_order")
    private Long idOrder;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @NotNull
    @Size(max = 4096)
    @Column(name = "description")
    private String description;

    @NotNull
    @Size(max = 4096)
    @Column(name = "order_state")
    private OrderState state;

    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    @DecimalMax(value = "1000.0", inclusive = true)
    @Column(name = "weight")
    double weight;

    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    @DecimalMax(value = "1000.0", inclusive = true)
    @Column(name = "volume")
    double volume;

    @NotNull
    @FutureOrPresent
    @Column(name = "start_date")
    LocalDate startDate;

    @NotNull
    @Future
    @Column(name = "receive_date")
    LocalDate receiveDate;

    public Order() {
    }

    public static class Builder {
        private Order newOrder;

        public Builder() {
            newOrder = new Order(this);
        }

        public Builder description(String desc) {
            newOrder.description = desc;
            return this;
        }

        public Builder weight(double weight) {
            newOrder.weight = weight;
            return this;
        }

        public Builder volume(double volume) {
            newOrder.volume = volume;
            return this;
        }

        public Builder state(OrderState state) {
            newOrder.state = state;
            return this;
        }

        public Builder startDate(LocalDate startDate) {
            newOrder.startDate = startDate;
            return this;
        }

        public Builder receiveDate(LocalDate receiveDate) {
            newOrder.receiveDate = receiveDate;
            return this;
        }

        public Builder user(User user) {
            newOrder.user = user;
            return this;
        }

        public Builder id(Long id) {
            newOrder.idOrder = id;
            return this;
        }

        public Order build() {
            return new Order(this);
        }
    }

    private Order(Builder builder) {
        description = builder.newOrder.getDescription();
        volume = builder.newOrder.getVolume();
        weight = builder.newOrder.getWeight();
        state = builder.newOrder.getState();
        startDate = builder.newOrder.getStartDate();
        receiveDate = builder.newOrder.getReceiveDate();
        user = builder.newOrder.getUser();
        idOrder = builder.newOrder.getIdOrder();
    }

    @Override
    public String toString() {
        return "Order{" +
                "idOrder=" + idOrder +
                ", user=" + user +
                ", description='" + description + '\'' +
                ", state=" + state +
                ", weight=" + weight +
                ", volume=" + volume +
                ", startDate=" + startDate +
                ", receiveDate=" + receiveDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Double.compare(order.weight, weight) == 0 &&
                Double.compare(order.volume, volume) == 0 &&
                Objects.equals(idOrder, order.idOrder) &&
                Objects.equals(user, order.user) &&
                Objects.equals(description, order.description) &&
                state == order.state &&
                Objects.equals(startDate, order.startDate) &&
                Objects.equals(receiveDate, order.receiveDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idOrder, user, description, state, weight, volume, startDate, receiveDate);
    }

    public Long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Long idOrder) {
        this.idOrder = idOrder;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OrderState getState() {
        return state;
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(LocalDate receiveDate) {
        this.receiveDate = receiveDate;
    }
}
