package com.ishostak.dope.services;

import com.ishostak.dope.Util.OrderState;
import com.ishostak.dope.excpetions.OrderServiceException;
import com.ishostak.dope.models.Order;
import com.ishostak.dope.models.User;
import com.ishostak.dope.repositories.OrderRepository;
import com.ishostak.dope.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    private final static Logger logger = LoggerFactory.getLogger(UserDataService.class);

    private OrderRepository orderRepository;
    private UserRepository userRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository, UserRepository userRepository) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
    }

    public Order create(Order order, Long id) {
        logger.info("Insert order into DB: order = {}", order);
        Optional<User> user = userRepository.findById(id);

        Order actualOrder = new Order.Builder()
                .description(order.getDescription())
                .startDate(order.getStartDate())
                .receiveDate(order.getReceiveDate())
                .state(OrderState.New)
                .user(user.get())
                .volume(order.getVolume())
                .weight(order.getWeight())
                .build();
        logger.debug("Insert order into DB: order = {} for customer {}", order, user);

        final Order insertedOrder = orderRepository.saveAndFlush(actualOrder);
        logger.debug("Order was inserted into DB: {}", insertedOrder);

        return insertedOrder;
    }

    @Transactional(readOnly = true)
    public List<Order> getAll() throws OrderServiceException {
        logger.info("Get all orders from DB");

        try {
            final List<Order> orderList = orderRepository.findAll();

            logger.debug("Orders were read from DB");

            return orderList;
        } catch (Exception ex) {
            logger.error("Error while getting orders from DB", ex);
            throw new OrderServiceException("An error occurred while getting orders", ex);
        }
    }

    @Transactional(readOnly = true)
    public List<Order> getAllForUser(Long id) throws OrderServiceException {
        logger.info("Get all orders for user ID: {} from DB", id);

        try {
            final List<Order> orderList = orderRepository.findByUserIdUser(id).get();

            logger.debug("Orders were read from DB");

            return orderList;
        } catch (Exception ex) {
            logger.error("Error while getting orders from DB", ex);
            throw new OrderServiceException("An error occurred while getting orders", ex);
        }
    }

    @Transactional(readOnly = true)
    public Order getOneForUser(Long orderId) throws OrderServiceException {
        logger.info("Getting orders with ID: {} from DB", orderId);

        try {
            final Order order = orderRepository.findById(orderId).get();

            logger.debug("Order was read from DB");

            return order;
        } catch (Exception ex) {
            logger.error("Error while getting order from DB", ex);
            throw new OrderServiceException("An error occurred while getting orders", ex);
        }
    }

    @Transactional
    public Order update(Order order, Long id) throws OrderServiceException {
        logger.info("Update order in DB: guid = {}, order = {}", id, order);

        final Order actualOrder = orderRepository.findById(id).get();

        try {
            actualOrder.setDescription(order.getDescription());
            actualOrder.setReceiveDate(order.getReceiveDate());
            actualOrder.setStartDate(order.getStartDate());
            actualOrder.setState(order.getState());
            actualOrder.setVolume(order.getVolume());
            actualOrder.setWeight(order.getWeight());
            actualOrder.setUser(order.getUser());

            Order updatedOrder = orderRepository.saveAndFlush(actualOrder);

            logger.debug("Order was updated in DB: {}", updatedOrder);

            return updatedOrder;
        } catch (Exception ex) {
            logger.error("Error while updating order in DB: " + order, ex);
            throw new OrderServiceException("An error occurred while updating order", ex);
        }
    }

    @Transactional
    public void deleteById(Long id) throws OrderServiceException {
        logger.info("Delete order from DB: id = {}", id);

        final Order actualOrder = orderRepository.getOne(id);

        try {
            orderRepository.deleteById(actualOrder.getIdOrder());
            orderRepository.flush();

            logger.debug("Order was deleted from DB: {}", actualOrder);
        } catch (Exception ex) {
            logger.error("Error while deleting order from DB: " + actualOrder, ex);
            throw new OrderServiceException("An error occurred while deleting order", ex);
        }
    }

    @Transactional
    public void deleteAll() throws OrderServiceException {
        logger.info("Delete all orders from DB");
        try {
            if (!(orderRepository.findAll().isEmpty())) {
                orderRepository.deleteAll();

                logger.debug("All orders was deleted from DB");
            }
            logger.debug("There is no orders in the DB");
        } catch (Exception ex) {
            logger.error("Error while deleting orders from DB: ", ex);
            throw new OrderServiceException("An error occurred while deleting orders", ex);
        }
    }

}
