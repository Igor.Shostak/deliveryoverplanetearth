package com.ishostak.dope.services;

import com.ishostak.dope.excpetions.AuthenticationServiceException;
import com.ishostak.dope.models.User;
import com.ishostak.dope.repositories.UserDataRepository;
import com.ishostak.dope.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class AuthenticationService {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationService.class);

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private UserDataRepository userDataRepository;
    private UserDataService userDataService;

    @Autowired
    public AuthenticationService(UserRepository userRepository, PasswordEncoder passwordEncoder, UserDataRepository userDataRepository, UserDataService userDataService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userDataRepository = userDataRepository;
        this.userDataService = userDataService;
    }

    @Transactional(readOnly = true)
    public void authenticateUser(String email, String key) throws AuthenticationServiceException {
        logger.info("Set authentication: {}", email);

        final Optional<User> user = userRepository.findByEmail(email);

        boolean valid = user.isPresent();

        if (valid) {
            logger.debug("User was found: {}", email);

            final String userKey = userDataRepository.findById(user.get().getIdUser()).get().getPassword();

            valid = passwordEncoder.matches(key, userKey);

            if (valid) {
                logger.debug("Password is valid for user {}", email);
            }
        }

        if (!valid) {
            logger.error("User is not valid: {}", email);
            throw new AuthenticationServiceException("Invalid email or password", HttpStatus.FORBIDDEN.value());
        }

        logger.debug("User is valid: {}", email);

        try {
            setAuthentication(user.get().getIdUser());
        } catch (Exception ex) {
            SecurityContextHolder.clearContext();

            logger.error("Error while authentication: " + email, ex);
            throw new AuthenticationServiceException("An error occurred while authentication", ex);
        }

        logger.info("User was authenticated successfully: {}", email);
    }

    private void validateUser(String email, String key) {

    }

    private void setAuthentication(Long id) {
        Collection<? extends GrantedAuthority> authorities = userDataService.getAuthorities(id);

        final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                id, null, authorities);

        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }
}
