package com.ishostak.dope.services;

import com.ishostak.dope.excpetions.UserServiceException;
import com.ishostak.dope.models.User;
import com.ishostak.dope.models.UserData;
import com.ishostak.dope.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;


@Service
public class UserService {
    private final static Logger logger = LoggerFactory.getLogger(UserService.class);

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public User create(String email, UserData data) {
        logger.info("Creating new user: {}", data);

        Optional<User> user = repository.findByEmail(email);

        if (user.isPresent()) {
            logger.error("User with email {} already exist in db", email);
            throw new UserServiceException("User already exists", HttpStatus.FORBIDDEN.value());
        }

        String encryptedPassword = passwordEncoder.encode(data.getPassword());
        data.setPassword(encryptedPassword);

        final User createdUser = new User(UUID.randomUUID(), data);
        createdUser.setEmail(email);

        try {
            logger.info("Inserting user to DB: {}", createdUser);
            repository.saveAndFlush(createdUser);
            logger.debug("User was inserted into DB: {}", createdUser);
        } catch (Exception ex) {
            logger.error("Error while inserting user into DB: " + createdUser, ex);
            throw new UserServiceException("An error occurred while inserting user", ex);
        }

        return createdUser;
    }

    @Transactional
    public User update(User newUser) {
        logger.info("Update user in DB: {}", newUser);

        User actualUser = repository.findByGuid(newUser.getGuid())
                .orElseThrow(() -> new NoSuchElementException("User was not found"));

        try {
            String encryptedPassword = passwordEncoder.encode(newUser.getData().getPassword());
            newUser.getData().setPassword(encryptedPassword);

            actualUser.setData(newUser.getData());
            actualUser.setEmail(newUser.getEmail());

            final User updatedUser = repository.saveAndFlush(actualUser);
            logger.debug("User was updated in DB: {}", updatedUser);

            return updatedUser;
        } catch (Exception ex) {
            logger.error("Error while updating user in DB: " + actualUser, ex);
            throw new UserServiceException("An error occurred while updating user", ex);
        }
    }

    @Transactional(readOnly = true)
    public User getByEmail(String email) throws UserServiceException {
        logger.info("Get user from DB by email: {}", email);

        final Optional<User> userFromDb = repository.findByEmail(email);

        return userFromDb.orElseThrow(() -> new UserServiceException("User was not found"));
    }

    @Transactional
    public void deleteAll() throws UserServiceException {
        logger.info("Delete all users except admin");
        try {
            repository.deleteAll();

            logger.debug("All users was deleted from DB");
        } catch (Exception ex) {
            logger.error("Error while deleting all users from DB:");
            throw new UserServiceException("An error occured while deleting all users", ex);
        }
    }

    @Transactional
    public void deleteByGuid(UUID guid) throws UserServiceException {
        logger.info("Delete user from DB by guid: {}", guid);

        final User user = repository.findByGuid(guid)
                .orElseThrow(() -> new NoSuchElementException("User was not found"));

        try {
            repository.deleteById(user.getIdUser());
            repository.flush();

            logger.debug("User was deleted from DB: {}", user);
        } catch (Exception ex) {
            logger.error("Error while deleting user from DB: " + user, ex);
            throw new UserServiceException("An error occurred while deleting user", ex);
        }
    }

    @Transactional(readOnly = true)
    public List<User> getAll() throws UserServiceException {
        logger.info("Get all users from DB");

        try {
            final List<User> users = repository.findAll();

            logger.debug("All users were gotten from DB");

            return (List<User>) users;
        } catch (DataAccessException ex) {
            logger.error("Error while getting all users from DB", ex);
            throw new UserServiceException("An error occurred while getting users", ex);
        }
    }
}
