package com.ishostak.dope.services;

import com.ishostak.dope.excpetions.UserServiceException;
import com.ishostak.dope.models.User;
import com.ishostak.dope.models.UserData;
import com.ishostak.dope.repositories.UserDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserDataService {
    private final static Logger logger = LoggerFactory.getLogger(UserDataService.class);

    private UserDataRepository userDataRepository;

    public UserDataService(UserDataRepository userDataRepository) {
        this.userDataRepository = userDataRepository;
    }

    public UserData create(UserData data) {
        logger.info("Writing user ID for user {}", data.getId());

        try {
            logger.debug("Inserting data to for user: {}", data.getId());
            userDataRepository.saveAndFlush(data);
        } catch (Exception ex) {
            logger.error("Error occurred while writing userdata for user:{}", data.getId());
            throw new UserServiceException("Error occurred while writing userdata for user:", ex);
        }
        return data;
    }

    @Transactional(readOnly = true)
    public Collection<? extends GrantedAuthority> getAuthorities(Long id) throws UserServiceException {
        logger.info("Get user authorities by email: {}", id);
        final List<GrantedAuthority> authorityList = new ArrayList<>();

        Optional<UserData> data = userDataRepository.findById(id);

        SimpleGrantedAuthority authority = new SimpleGrantedAuthority("User");

        authorityList.add(authority);

        if (data.isPresent() && data.get().isManager()) {
            authorityList.add(new SimpleGrantedAuthority("Manager"));
        }

        return authorityList;
    }

}
