package com.ishostak.dope.Util;

public enum OrderState {
    New,
    Delivering,
    Waiting,
    Completed,
    Canceled;
}
