package com.ishostak.dope.excpetions;

import javax.validation.constraints.NotNull;

public class OrderServiceException extends ServiceException {
    public OrderServiceException(String message) {
        super(message);
    }

    public OrderServiceException(@NotNull String message, @NotNull Exception ex) {
        super(message, ex);
    }

    public OrderServiceException(Throwable cause) {
        super(cause);
    }

    public OrderServiceException(String message, int errorCode) {
        super(message, errorCode);
    }

    public OrderServiceException(@NotNull Exception ex, @NotNull String error, @NotNull int errorCode, @NotNull String message) {
        super(ex, error, errorCode, message);
    }
}
