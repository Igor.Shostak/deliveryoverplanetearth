package com.ishostak.dope.excpetions;

public class AuthenticationServiceException extends ServiceException {

    public AuthenticationServiceException(String message, Exception ex) {
        super(message, ex);
    }

    public AuthenticationServiceException(String message, int errorCode) {
        super(message, errorCode);
    }
}