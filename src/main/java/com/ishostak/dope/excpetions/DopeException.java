package com.ishostak.dope.excpetions;

public class DopeException extends RuntimeException {
    public DopeException() {
    }

    public DopeException(String message) {
        super(message);
    }

    public DopeException(String message, Throwable cause) {
        super(message, cause);
    }

    public DopeException(Throwable cause) {
        super(cause);
    }
}
