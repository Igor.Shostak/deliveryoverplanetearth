package com.ishostak.dope.excpetions;

import javax.validation.constraints.NotNull;

public class UserServiceException extends ServiceException {

    public UserServiceException(String message) {
        super(message);
    }

    public UserServiceException(@NotNull String message, @NotNull Exception ex) {
        super(message, ex);
    }

    public UserServiceException(Throwable cause) {
        super(cause);
    }

    public UserServiceException(String message, int errorCode) {
        super(message, errorCode);
    }
}
