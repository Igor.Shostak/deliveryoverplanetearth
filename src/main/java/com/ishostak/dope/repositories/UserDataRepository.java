package com.ishostak.dope.repositories;

import com.ishostak.dope.models.UserData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Repository for work with UserData
 */
public interface UserDataRepository extends JpaRepository<UserData, Long> {

    /**
     *
     * @param id
     * @return userdata for user email or {@literal Optional#empty()}
     */
    Optional<UserData> findById(Long id);
}
