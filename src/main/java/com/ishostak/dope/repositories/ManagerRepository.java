package com.ishostak.dope.repositories;

import com.ishostak.dope.models.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for work with Managers
 */
public interface ManagerRepository/* extends JpaRepository<Manager, Long> */{

}
