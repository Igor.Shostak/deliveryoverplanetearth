package com.ishostak.dope.repositories;

import com.ishostak.dope.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for work with Orders
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

    /**
     * Retrieves all orders for user
     *
     * @param userId
     * @return the user with the given guid or {@literal Optional#empty()} if none found
     */
     Optional<List<Order>> findByUserIdUser(Long userId);

    /**
     * Retrieves all orders for user
     *
     * @param orderId
     * @return the user with the given guid or {@literal Optional#empty()} if none found
     */
    Optional<Order> findById(Long orderId);



}
