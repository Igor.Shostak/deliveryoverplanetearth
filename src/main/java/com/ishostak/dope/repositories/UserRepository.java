package com.ishostak.dope.repositories;

import com.ishostak.dope.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * Repository for work with Users
 */
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Retrieves a user by its guid.
     *
     * @param guid
     * @return the user with the given guid or {@literal Optional#empty()} if none found
     */
    Optional<User> findByGuid(UUID guid);


    /**
     * Retrieves a user by its ID.
     *
     * @param id
     * @return the user with the given guid or {@literal Optional#empty()} if none found
     */
    Optional<User> findById(Long id);

    /**
     * Retrieves a user by its email.
     *
     * @param email
     * @return the user with the given email
     */
    Optional<User> findByEmail(String email);
}
