package com.ishostak.dope.dto;

import com.ishostak.dope.Util.Validation.ValidEmail;
import com.ishostak.dope.Util.Validation.ValidPassword;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegistrationDto {

    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    @Size(max = 256)
    private String nickname;

    @NotNull
    @NotEmpty
    private boolean isManager;

    @NotNull
    @NotEmpty
    @Size(max = 256)
    @ValidEmail
    private String email;

    @NotNull
    @NotEmpty
    @Size(min = 6, max = 32)
    @ValidPassword
    private String password;


    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public boolean isManager() {
        return isManager;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
