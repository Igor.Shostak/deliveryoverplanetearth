package com.ishostak.dope.dto;

import com.ishostak.dope.Util.Validation.ValidEmail;
import com.ishostak.dope.Util.Validation.ValidPassword;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

public class LoginDto {

    @NotNull
    @NotEmpty
    @Size(max = 256)
    @ValidEmail
    String email;

    @NotNull
    @NotEmpty
    @Size(min = 6, max = 32)
    @ValidPassword
    String password;

    public LoginDto() {
    }

    public LoginDto(@NotNull @NotEmpty @Size(max = 256) String email, @NotNull @NotEmpty @Size(min = 6, max = 32) String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginDto loginDto = (LoginDto) o;
        return Objects.equals(email, loginDto.email) &&
                Objects.equals(password, loginDto.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password);
    }

    @Override
    public String toString() {
        return "LoginDto{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
