package com.ishostak.dope.controllers;

import com.ishostak.dope.models.Order;
import com.ishostak.dope.models.User;
import com.ishostak.dope.services.OrderService;
import com.ishostak.dope.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    private OrderService orderService;
    private UserService userService;

    @Autowired
    public OrderController(OrderService orderService, UserService userService) {
        this.orderService = orderService;
        this.userService = userService;
    }

    /**
     * Returns information about all orders for user
     *
     * @param id order id from the URN
     * @return Response Entity with {@link List<Order>} object
     */

    @GetMapping(path = "/{id}")
    public ResponseEntity getOrderList(@PathVariable Long id) {
        logger.info("Client: {} requested the list of all orders", id);

        List<Order> orders = orderService.getAllForUser(id);

        logger.info("Sending list of all orders to the client:\n{}", orders);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(orders);
    }

    /**
     * Returns information about the requested order
     *
     * @param orderId order guid from the URN
     * @return Response Entity with {@link Order} object
     */
    @GetMapping(path = "/order/{orderId}")
    public ResponseEntity getOrder(@PathVariable Long orderId) {
        logger.info("Client requested the order {}", orderId);

        Order order = orderService.getOneForUser(orderId);

        logger.info("Sending the order {} to the client", order);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(order);
    }

    /**
     * Creates a new order
     *
     * @param order list of {@link Order} object
     * @return Response entity with list of {@link Order} object
     */
    @PostMapping()
    public ResponseEntity<Order> addOrder(@RequestBody Order order) {
        logger.info("Accepted requested to create a new order:\n{}", order);


        final String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final User user = userService.getByEmail(email);

        Order createdOrder = orderService.create(order, user.getIdUser());

        logger.info("Sending the created order to the client:\n{}", createdOrder);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(createdOrder);
    }

    /**
     * Modifies information of the specified order
     *
     * @param id order id from the URN
     * @param order order object
     * @return Response entity with modified {@link Order} object
     */
    @PutMapping("/order/{orderId}")
    public ResponseEntity<Order> modifyOrder(@PathVariable Long id, @RequestBody Order order) {
        logger.info("Accepted modified order {} from the client", order);

        Order modifiedOrder = orderService.update(order, id);

        logger.info("Sending the modified order {} to the client", modifiedOrder);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(modifiedOrder);
    }

    /**
     * Removes the specified order
     *
     * @param id order guid from the URN
     */
    @DeleteMapping("/order/{orderId}")
    public ResponseEntity deleteOrder(@PathVariable Long id) {
        logger.info("Accepted request to delete the order {}", id);

        orderService.deleteById(id);

        logger.info("Order {} successfully deleted", id);
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .build();
    }
}
