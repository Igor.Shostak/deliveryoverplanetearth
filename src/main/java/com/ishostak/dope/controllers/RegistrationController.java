package com.ishostak.dope.controllers;

import com.ishostak.dope.dto.RegistrationDto;
import com.ishostak.dope.models.UserData;
import com.ishostak.dope.services.AuthenticationService;
import com.ishostak.dope.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@RestController
public class RegistrationController {
    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    private UserService userService;
    private AuthenticationService authenticationService;

    @Autowired
    public RegistrationController(UserService userService, AuthenticationService authenticationService) {
        this.userService = userService;
        this.authenticationService = authenticationService;
    }

    @GetMapping(path = "/registration")
    public String login(Model model) {
        model.addAttribute("message", "Thank you for visiting.");
        return "registration";
    }

    @PostMapping(path = "/registration")
    public ResponseEntity registration(@RequestBody @Valid RegistrationDto data) {
        logger.info("Creating request for user registration: {}", data.getEmail());
        UserData registrationData = new UserData();

        registrationData.setManager(data.isManager());
        registrationData.setLastLogin(LocalDate.now());
        registrationData.setPassword(data.getPassword());

        userService.create(data.getEmail(), registrationData);

        authenticationService.authenticateUser(data.getEmail(), data.getPassword());

        return ResponseEntity
                .status(HttpStatus.OK)
                .build();
    }
}

