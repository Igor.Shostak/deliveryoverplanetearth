package com.ishostak.dope.controllers;

import com.ishostak.dope.dto.LoginDto;
import com.ishostak.dope.services.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class LoginController {
    private final static Logger logger = LoggerFactory.getLogger(LoginController.class);

    private AuthenticationService service;

    @Autowired
    public LoginController(AuthenticationService service) {
        this.service = service;
    }

    @PostMapping(path = "/login")
    public ResponseEntity login(@RequestBody @Valid LoginDto loginData) {
        logger.info("Try to sign in: {}", loginData.getEmail());

        service.authenticateUser(loginData.getEmail(), loginData.getPassword());

        return ResponseEntity.status(HttpStatus.OK).body("redirect:profile");
    }
}
